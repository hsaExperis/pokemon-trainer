import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent {
  usernameInput = "";

  @Output() loginEvent = new EventEmitter<string>();

  onSubmit() {
    this.loginEvent.emit(this.usernameInput);
  }
}
