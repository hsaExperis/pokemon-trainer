import { Component } from "@angular/core";
import { User } from "../models/user.model";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})

export class LoginComponent {
  title1 = 'Pokemon';
  title2 = 'Trainer';

  constructor(
    private readonly authService: AuthService
  ) { }

  loginUser(username: string): void {
    const user: User = {
      name: username,
    };
    this.authService.LogIn(user);
  }
}