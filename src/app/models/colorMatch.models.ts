export interface ColorMatchingObject {
    bug:        string;
    dragon:     string;
    electric:   string;
    fighting:   string;
    fire:       string;
    flying:     string;
    ghost:      string;
    grass:      string;
    ground:     string;
    ice:        string;    
    normal:     string;
    poison:     string;
    psychic:    string;
    rock:       string;
    water:      string;
    fairy:      string;
    steel:      string;
    dark:       string;
  }