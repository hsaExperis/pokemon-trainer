export interface Pokemon {
    id: number;
    name: string;
    sprites: Sprites;
    types: TypeContainer[];
    isCaptured: boolean;
}

export interface Sprites {
    front_default: string;
}

export interface TypeContainer {
    type: Type;
}

export interface Type {
    name: string;
}