import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { forkJoin, Observable } from "rxjs";
import {finalize, map,tap} from 'rxjs/operators'
import { Pokemon } from "../models/pokemon.model";

@Injectable({
    providedIn: "root",
})

export class PokemonService {
    private pokemons: Pokemon[] = [];
    private error: string = "";

    constructor(private readonly http: HttpClient) {
        this.pokemons = JSON.parse(localStorage.getItem("allPokemons") ?? "[]")
     }

    public fetchPokemon(): void {
        const getRequests: Observable<Pokemon>[] = [];

        for (let i = 1; i <= 151; i++) {
            getRequests.push(this.http.get<Pokemon>("https://pokeapi.co/api/v2/pokemon/" + i))
        }

        forkJoin(getRequests)
        .pipe(
            tap(originalData => console.log(originalData)),
            map((pokemonArray) => {
                return pokemonArray.map(pokemon => {
                    const minifiedPokemon: Pokemon = {
                        id: pokemon.id,
                        name: pokemon.name,
                        types: pokemon.types,
                        sprites: pokemon.sprites,
                        isCaptured: false,
                    };
                    return minifiedPokemon;
                })
            })
        )
        .subscribe({
            next: (pokemons) => {
                this.pokemons = pokemons;
                localStorage.setItem("allPokemons", JSON.stringify(pokemons));
            },
        });
    }

    public getPokemons(): Pokemon[] {
        return this.pokemons;
    }
}