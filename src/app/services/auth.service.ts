import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;

  constructor(public router: Router) { 
    this.isInLocalStorage();
  }

  private isInLocalStorage(): boolean {
    if (localStorage.getItem("token") === "valid") {
      this.userData = JSON.parse(localStorage.getItem("currentUser") ?? "");
      return true;
    } else {
      return false;
    }
  }

  get isLoggedIn(): boolean {
    return localStorage.getItem("token") === "valid";
  }

  LogIn(user: User):void {
    localStorage.setItem("currentUser", JSON.stringify(user));
    localStorage.setItem("token", "valid");
    localStorage.setItem("pokemons", JSON.stringify([]));
    this.router.navigate(["catalogue"]);
  }

  LogOut() {
    localStorage.clear();
    this.router.navigate(["login"]);
  }
}
