import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(private readonly router:Router, private readonly authService:AuthService) { }

  toTrainer(): void {
    this.router.navigate(["trainer"]);
  }

  toCatalogue(): void {
    this.router.navigate(["catalogue"]);
  }

  logOut(): void {
    this.authService.LogOut();
  }

  get getIsLoggedIn(): boolean {
    return this.authService.isLoggedIn;
  }

}
