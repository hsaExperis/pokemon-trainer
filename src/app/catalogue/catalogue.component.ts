import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

  constructor(private readonly pokemonService: PokemonService) { }

  savedPokemons: Pokemon[] = [];

  ngOnInit(): void {
    this.savedPokemons = JSON.parse(localStorage.getItem("pokemons") ?? "");
  }

  savePokemon(event: any) {
    const pokemonClicked = this.pokemons.find((pokemon) => pokemon.name === event.currentTarget.id);

    if (pokemonClicked !== undefined) {
      if (this.savedPokemons.some(pokemon => pokemon.id == pokemonClicked.id)) {
        this.savedPokemons = this.savedPokemons.filter(pokemon => pokemon.id !== pokemonClicked.id);
      } else {
        this.savedPokemons.push(pokemonClicked);
      }
      localStorage.setItem("pokemons", JSON.stringify(this.savedPokemons));
    }
  }

  get pokemons(): Pokemon[] {

    return this.pokemonService.getPokemons().map(pokemonAPI => {
      if (this.savedPokemons.some(pokemon => pokemon.id === pokemonAPI.id)) {
        pokemonAPI.isCaptured = true;
        return pokemonAPI
      } else {
        pokemonAPI.isCaptured = false;
        return pokemonAPI;
      }
    });
  }
}
