import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit {
  pokemons: Pokemon[] = [];
  constructor() { }

  ngOnInit(): void {
    this.pokemons = JSON.parse(localStorage.getItem("pokemons") ?? "");
  }

}
