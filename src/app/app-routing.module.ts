import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { SecureInnerPagesGuard } from './auth/secure-inner-pages.guard';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { LoginComponent } from './login/login.component';
import { TrainerComponent } from './trainer/trainer.component';

const routes: Routes = [
  {
    path: "",
    redirectTo:"/login",
    pathMatch: "full",
  },
  {
    path: "login",
    component: LoginComponent,
    canActivate: [SecureInnerPagesGuard],
  },
  {
    path: "catalogue",
    component: CatalogueComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "trainer",
    component: TrainerComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
