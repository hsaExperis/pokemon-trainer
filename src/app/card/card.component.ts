import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { ColorMatchingObject } from '../models/colorMatch.models';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  backgroundColor = "";
  imgBackgroundColor = "";

  private colorMatchingObject: Record<string, string> = {
    "bug": "#a8b821",
    "dragon": " #7038f9",
    "electric": "#f8d030",
    "fighting": "#c03028",
    "fire": "#f07f2f",
    "flying": "#a890f0",
    "ghost": "#705798",
    "grass": "#78c84f",
    "ground": "#e0c069",
    "ice": "#98d8d8",
    "normal": "#a9a878",
    "poison": "#a040a1",
    "psychic": "#f85888",
    "rock": "#b7a038",
    "water": "#6890f0",
    "fairy": "#f7c9e3",
    "steel": "#b8b8d0",
    "dark": "#6f5848",
  }

  @Input() pokemon?: Pokemon;
  @Output() clickEvent = new EventEmitter<any>();
  @Input() showBall: boolean = false;

  constructor() { }
  ngOnInit(): void {
    if (this.pokemon) {

      const color = this.colorMatchingObject[this.pokemon.types[0].type.name];
      this.backgroundColor = color;
      this.imgBackgroundColor = this.shadeColor(color, -40)

    }
  }

  savePokemon(event: any) {
    this.clickEvent.emit(event)
  }

  shadeColor(color: string, percent: number) {

    let R = parseInt(color.substring(1, 3), 16);
    let G = parseInt(color.substring(3, 5), 16);
    let B = parseInt(color.substring(5, 7), 16);

    R = Math.round(R * (100 + percent) / 100);
    G = Math.round(G * (100 + percent) / 100);
    B = Math.round(B * (100 + percent) / 100);

    R = (R < 255) ? R : 255;
    G = (G < 255) ? G : 255;
    B = (B < 255) ? B : 255;

    let RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
    let GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
    let BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

    return "#" + RR + GG + BB;
  }
}
