import { Component, OnInit } from '@angular/core';
import { Pokemon } from './models/pokemon.model';
import { PokemonService } from './services/pokemon.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private readonly pokemonService: PokemonService) {}

  ngOnInit(): void {
    if (this.pokemons.length === 0) {
      this.pokemonService.fetchPokemon();
    }
  }

  get pokemons(): Pokemon[] {
    return this.pokemonService.getPokemons();
  }
}
